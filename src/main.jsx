import React from "react";
import ReactDOM from "react-dom/client";
import MainApp from "./components/app";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import "./styles/main.css";

function App() {
  return (
    <div>
      <BrowserRouter>
        <Routes>
          <Route index element={<MainApp />} />
          <Route path="*" element={<p>Page Not Found !!</p>} />
        </Routes>
      </BrowserRouter>
    </div>
  );
}

ReactDOM.createRoot(document.getElementById("root")).render(
  <React.StrictMode>
    <App />
  </React.StrictMode>
);
