import { Button } from "react-bootstrap";

import style from "../styles/herosection.module.css";
function Herosection() {
  return (
    <div className="container">
      <div className="row">
        <div className="cl-12">
          <div>
            <h1 className={style.main_heading}>
              Savor Nature’s Best with <span>GreenBite</span> <br /> Organic{" "}
              <span>Deliver</span>
            </h1>
            <p className={style.second_heading}>
              GreenBite delivers the freshest, organic produce and wholesome
              meal kits to your <br /> doorstep. Elevate your well-being through
              our commitment to health, sustainability, <br /> and convenience.
            </p>
            <Button variant="success" className={style.cta_btn}>
              Order Now
            </Button>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Herosection;
