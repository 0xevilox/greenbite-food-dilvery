import { Button, Card } from "react-bootstrap";
import style from "../styles/product.module.css";
function Product() {
  return (
    <div className="container">
      <div className="row">
        <div className="col-12">
          <Card className={style.card}>
            <Card.Img
              style={{ position: "relative", bottom: "80px" }}
              variant="top"
              src="/Products/product_2.png"
            />
            <Card.Body className={style.Content}>
              <Card.Title className={style.ProductName}>
                Green Harmony Box
              </Card.Title>
              <Card.Text className={style.discription}>
                A delightful assortment of fresh, organic vegetables for
                wholesome meals.
              </Card.Text>
              <Button variant="success" className={style.order_btn}>
                ORDER NOW
              </Button>
            </Card.Body>
          </Card>
        </div>
      </div>
    </div>
  );
}

export default Product;
