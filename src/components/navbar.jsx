import style from "../styles/navbar.module.css";
function Navbar() {
  return (
    <div className="container">
      <div className="row">
        <div className="cl-12">
          <header className={style.container}>
            <h1 className="Logo">Green Bite</h1>
            <nav>
              <ul className={style.nav}>
                <li>Home</li>
                <li>About</li>
                <li>Product</li>
                <li>Testimonials</li>
                <li>Contact</li>
              </ul>
            </nav>
          </header>
        </div>
      </div>
    </div>
  );
}

export default Navbar;
