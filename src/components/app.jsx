//Third-Party Module
import "bootstrap/dist/css/bootstrap.min.css";
import "bootstrap/dist/js/bootstrap.bundle.min.js";
import { Swiper, SwiperSlide } from "swiper/react";
import { Autoplay, Pagination } from "swiper/modules";
import "swiper/css";
import "swiper/css/pagination";

//Internal Module
import Navbar from "./navbar";
import Herosection from "./herosection";
import Greendeal from "./greendeal";
import { Categories_title, Testimonial_title, Product_title } from "./titles";
import Categories from "./categories";
import Product from "./product";
import Testionmonial from "./testiomonial";
import Footer from "./footer";
function MainApp() {
  return (
    <div>
      <div className="backgroundimg">
        <Navbar />
        <Herosection />
      </div>
      <Greendeal />
      <Swiper
        pagination={{
          dynamicBullets: false,
        }}
        modules={[Pagination]}
      >
        <Categories_title />
        <div className="tomatoimg"></div>
        <SwiperSlide>
          <div className="container d-flex">
            <Categories />
            <Categories />
            <Categories />
          </div>
        </SwiperSlide>
        <SwiperSlide>
          <div className="container d-flex">
            <Categories />
            <Categories />
            <Categories />
          </div>
        </SwiperSlide>
      </Swiper>
      <Product_title />
      <div className="container d-flex">
        <Product />
        <Product />
        <Product />
      </div>
      <div className="breaker"></div>
      <Swiper
        autoplay={{ delay: 3000, disableOnInteraction: false }}
        modules={[Autoplay]}
      >
        <Testimonial_title />
        <div className="followerimg"></div>
        <SwiperSlide>
          <div className="container d-flex">
            <Testionmonial />
            <Testionmonial />
            <Testionmonial />
          </div>
        </SwiperSlide>
        <SwiperSlide>
          <div className="container d-flex">
            <Testionmonial />
            <Testionmonial />
            <Testionmonial />
          </div>
        </SwiperSlide>
        <SwiperSlide>
          <div className="container d-flex">
            <Testionmonial />
            <Testionmonial />
            <Testionmonial />
          </div>
        </SwiperSlide>
        <SwiperSlide>
          <div className="container d-flex">
            <Testionmonial />
            <Testionmonial />
            <Testionmonial />
          </div>
        </SwiperSlide>
      </Swiper>
      <div className="breaker"></div>
      <div className="footerimg">
        <Footer />
      </div>
    </div>
  );
}

export default MainApp;
