import { Card } from "react-bootstrap";

import style from "../styles/testiomonial.module.css";
function Testionmonial() {
  return (
    <div className="container">
      <div className="row">
        <div className="col-12">
          <Card
            className={style.card_container}
            style={{ width: "370px", height: "300px" }}
          >
            <Card.Body>
              <Card.Title>
                <img src="/Icons/quotes.png" alt="quotes" />
                <div className="star">
                  <img src="/Icons/star.svg" alt="rating star" />
                  <img src="/Icons/star.svg" alt="rating star" />
                  <img src="/Icons/star.svg" alt="rating star" />
                  <img src="/Icons/star.svg" alt="rating star" />
                  <img src="/Icons/empty_star.svg" alt="rating star" />
                </div>
              </Card.Title>
              Green Harmony Box transformed my cooking experience. The fresh
              veggies are a game- changer! I look forward to my surprise
              <br /> assortment every week.
              <div className="py-4">
                <img src="/Users/users.svg" alt="Testionmonial Users" />
                <div className={style.user}>
                  <p className={style.username}>Rishub</p>
                  <p className={style.status}>College Student</p>
                </div>
              </div>
            </Card.Body>
          </Card>
        </div>
      </div>
    </div>
  );
}

export default Testionmonial;
