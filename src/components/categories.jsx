import { Arrow } from "./titles";
import style from "../styles/categories.module.css";

function Categories() {
  return (
    <div className="container">
      <div className="row">
        <div className="col-12">
          <div className={style.product_container}>
            <div className={style.product_data}>
              <img src="/Products/pro.png" alt="Product Name" />
              <h1 className={style.title}>Quick & Easy Delights</h1>
            </div>
            <Arrow />
          </div>
        </div>
      </div>
    </div>
  );
}

export default Categories;
