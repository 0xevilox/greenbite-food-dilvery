export function Categories_title() {
  return (
    <div className="container text-center">
      <h1
        style={{
          fontSize: "40px",
          fontStyle: "normal",
          fontWeight: "700",
          position: "relative",
          top: "10px",
        }}
      >
        CATEGORIES
      </h1>
    </div>
  );
}

export function Testimonial_title() {
  return (
    <div className="container text-center">
      <h1
        style={{
          fontSize: "40px",
          fontStyle: "normal",
          fontWeight: "700",
        }}
      >
        TESTIMONIALS
      </h1>
    </div>
  );
}

export function Product_title() {
  return (
    <div className="container text-center">
      <h1
        style={{
          fontSize: "40px",
          fontStyle: "normal",
          fontWeight: "700",
          marginBottom: "100px",
          position: "relative",
          top: "40px",
        }}
      >
        PRODUCTS
      </h1>
    </div>
  );
}
export function Arrow() {
  return (
    <div className="circle">
      <img
        style={{ padding: "8px" }}
        src="Icons/arrow_back.svg"
        alt="Arrow Button"
      />
    </div>
  );
}
