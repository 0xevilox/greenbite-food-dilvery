import style from "../styles/greeendeal.module.css";
function Greendeal() {
  return (
    <div className="container">
      <div className="row">
        <div className="cl-12">
          <ul className={style.list}>
            <li className={style.list_in}>
              <img
                className={style.proimg}
                src="/Icons/icons-1.png"
                alt="Eco-friendly packing"
              />
              <p>
                Eco-friendly <br /> Packaging
              </p>
            </li>
            <li className={style.list_in}>
              <img
                className={style.proimg}
                src="/Icons/icons-2.png"
                alt="Eco-friendly packing"
              />
              <p>
                Free Home <br /> delivery
              </p>
            </li>
            <li className={style.list_in}>
              <img
                className={style.proimg}
                src="/Icons/icons-3.png"
                alt="Eco-friendly packing"
              />
              <p>
                20% Discount on <br />
                all food{" "}
              </p>
            </li>
          </ul>
          <div className={style.line_breaker_green}></div>
          <div className={style.line_breaker}></div>
        </div>
      </div>
    </div>
  );
}

export default Greendeal;
