import style from "../styles/footer.module.css";
function Footer() {
  return (
    <div className="container">
      <div className="row">
        <div className="col-12 d-flex" style={{ gap: "400px" }}>
          <div className={style.side_1}>
            <h1 className="Logo">GREEN BITE</h1>
            <p>
              Welcome to GreenBite, where freshness meets flavor in every <br />
              bite. We{"'"}re on a mission to deliver organic goodness to your
              <br />
              doorstep, sourced directly from local farmers. Join us in savoring
              <br />
              the joy of mindful and nourishing eating.
            </p>

            <p>
              <img src="/Icons/email.png" alt="email" /> helpline@greenbite.in
            </p>
            <p>
              <img src="/Icons/ph.png" alt="Phone Number" />
              949858488383838
            </p>
          </div>
          <div className={style.side_2}>
            <h1 className={style.follow}>Follow Us</h1>
            <img width={"50px"} src="/Icons/facebook.png" alt="facebook" />
            <img
              width={"50px"}
              className="mx-2"
              src="/Icons/instagram.png"
              alt="facebook"
            />
            <img width={"40px"} src="/Icons/linkedin.png" alt="facebook" />
          </div>
        </div>
      </div>
      <div className={style.line_breaker}></div>
      <footer className="text-center text-white py-3">
        Copyright to GreenBite
      </footer>
      <div
        className="d-flex justify-content-xl-end"
        style={{ gap: "10px", color: "white" }}
      >
        <p>Term of Services</p>
        <p>Privacy Policy</p>
      </div>
    </div>
  );
}

export default Footer;
